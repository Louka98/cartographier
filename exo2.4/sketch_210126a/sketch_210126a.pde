 PImage carte;
Table pos;
int lignes;

Table donnees,famille;
float dmin = MAX_FLOAT;
float dmax = MIN_FLOAT;

Integrator[] interp,mais;


void setup() {
  
    
    size(866, 803);
    carte = loadImage("region.png");
    pos = new Table("position.tsv");
    lignes = pos.getRowCount();
    donnees = new Table("fans.tsv");
    famille= new Table("maisons.tsv");
    
      
     PFont font = loadFont("Arial-ItalicMT-48.vlw");
     textFont(font);     // Spécifie la police à utiliser par la suite.
     textAlign(CENTER);
     frameRate(30);
          
          
         
          
          
          
      // On charge les valeurs initiales dans les interpolateurs :
      
      interp = new Integrator[lignes];
      mais=new Integrator[lignes];
      for(int ligne = 0; ligne < lignes; ligne++) {
        interp[ligne] = new Integrator(donnees.getFloat(ligne, 1));
        mais[ligne]=new Integrator(famille.getFloat(ligne,1));
    }

    noStroke();

      
     
    trouverMinMax();
    
   


}

void draw() {
    background(255);
    image(carte, 0, 0);

    smooth();
    fill(192, 0, 0);
    noStroke();
    
    textSize(32);

    for(int ligne = 0; ligne < lignes; ligne++) {
      
      interp[ligne].update();
      mais[ligne].update();
        // La clé nous sera utile pour retrouver les valeurs dans l'autre
        // ensemble de données.
        String cle = donnees.getRowName(ligne);
        float x = pos.getFloat(cle, 1);
        float y = pos.getFloat(cle, 2);
         float valeur = interp[ligne].value;
        float couleur=mais[ligne].value;
        dessinerDonnees(x, y, cle,ligne);
        
        if(couleur==1){
           printl(x,y,"Barathéon",valeur);
        
        }
        
           if(couleur==2){
             
             printl(x,y,"Targaryen",valeur);
        
        }
        
           if(couleur==3){
             
             printl(x,y,"Stark",valeur);
        
        }
        
           if(couleur==4){
             
             printl(x,y,"Lannister",valeur);
        
        }
    }
    
//Affiche une légende en bas de la carte
    
    textSize(20);
    
String s = "Carte des régions de france qui montre le nombre de fans de la série Game of Thrones selon la grandeur de la bulle et la famille préféré par la région selon la couleur de la bulle ";
fill(50);
text(s, 70, 720    , 750, 250);
    
    
    
}


void dessinerDonnees(float x, float y, String cle,int ligne) {
    // Croise la donnée avec la position.
    float valeur = interp[ligne].value;
    float couleur=mais[ligne].value;
    // On transforme la valeur de son intervalle de définition vers l'intervalle [2,40].
    // La fonction map est prédéfinie par Processing.
    float taille = map(valeur, dmin, dmax, 2, 40);
    // Enfin on dessine une ellipse dont la taille varie en fonction de la valeur.
    
    //fan de la maison Barathéon
    if(couleur==1){
      fill(#f2330b);
    ellipse(x, y, taille, taille);
    }
    
    //fans de la maison Targaryen
    
    if(couleur==2){
      fill(#080302);
    ellipse(x, y, taille, taille);
    }
    
    //fans de la maison Stark
    
    if(couleur==3){
      fill(#13e50c);
    ellipse(x, y, taille, taille);
    }
    
    // fans de la maison Lannister
    
    if(couleur==4){
      fill(#160ce5);
    ellipse(x, y, taille, taille);
    }
}





//Affiche le nombre de fans et le nom de la famille préféré de chaques régions 

void printl(float xx,float yy,String famille,float fans){
 if(dist(xx, yy, mouseX, mouseY)<10){
    
    
   String s = "maison "+" "+famille;
   
   String s1="fans"+" "+ fans ;
   
  
   
    
    textSize(25);
    text(s, xx, yy-20); 
    fill(0, 102, 153);
    text(s1, xx, yy+30);

    
 }
}

void keyPressed() {
    if(key == ' ')
        majDonnees();
        
   
    
    
    
    
        
   
        

    
}


void majDonnees() {
    for(int ligne = 0; ligne < lignes; ligne++) {
        
       interp[ligne].target(random(0, 10));
       //mais[ligne].target(int((random(1,5))));
       
    }
}




void trouverMinMax() {
    for(int ligne = 0; ligne < lignes; ligne++) {
        float valeur = donnees.getFloat(ligne, 1);
        if(valeur > dmax) dmax = valeur;
        if(valeur < dmin) dmin = valeur;
    }
}
